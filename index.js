const axios = require("axios");
const notifier = require("node-notifier");
var oldValue

var getStatus = function() {
    console.log("Polling for status")
    axios.get("https://das-labor.org/status/status.php?status")
        .then(response => {
            console.log(`Response: ${response.data}`);
            if (response.data != oldValue){
                notifier.notify({
                    title: `Labor is now: ${response.data}`,
                    message: `Das Labor is now: ${response.data}`
                })
                oldValue = response.data;
            }
        })
        .catch(err => {
            console.error("Error: ", err);
        });
}
getStatus();
setInterval(getStatus, 60000)
